
CREATE TABLE movie(
    movie_id INTEGER PRIMARY KEY,
    movie_title VARCHAR(30),
    movie_release_date date,
    movie_time int,
    director_name VARCHAR(30)
);

